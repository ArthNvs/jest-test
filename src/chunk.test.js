const chunckArray = require('./chunk')


// model test with fuction exists
test('chunckArray function exists', () =>{
  expect(chunckArray).toBeDefined();
});

test('chunck an Array of 10 values with length os 2', () =>{
  const numbers = [1,2,3,4,5,6,7,8,9,10];
  const len = 2;
  const chunckArr = chunckArray(numbers, len);

  expect(chunckArr).toEqual([[1,2],[3,4],[5,6],[7,8],[9,10]]);
})
