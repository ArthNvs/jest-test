const reverseString = require('./reversestring');

// model test with fuction exists
test('reveseString function exists', () =>{
    expect(reverseString).toBeDefined();
});
// model test function rigth
test('String reverses', () =>{
    expect(reverseString('hello')).toEqual('olleh')
});

// model test function rigth
test('String reverses', () =>{
    expect(reverseString('Hello')).toEqual('olleh')
})