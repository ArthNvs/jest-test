const url = process.env.MONGO_CONNECTION;
const { MongoClient } = require('mongodb');

describe('insert', () => {
  let connection;
  let db;

  beforeAll(async () => {
    connection = await MongoClient.connect(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    db = await connection.db(process.env.DATABASE_NAME);
  });

  afterAll(async () => {
    await connection.close();
    await db.close();
  });

  it('should insert a doc into collection', async () => {
    const users = db.collection('metrics');

    const mockUser = await { _id: '0', name: 'John' };
    await users.insertOne(mockUser);

    const insertedUser = await users.findOne({ _id: '0' });
    expect(insertedUser).toEqual(mockUser);
  });
});
