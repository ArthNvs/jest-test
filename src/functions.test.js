const functions = require('./functions');


// model simple test
test('Adds 2 + 2 to equal 4', () =>{
    expect(functions.add(2,2)).toBe(4);
});

// model simple test NOT
test('Adds 2 + 2 to NOT equal 5', () =>{
    expect(functions.add(2,2)).not.toBe(5);
});

// model simple test NULL
test('Should be null', () =>{
    expect(functions.isNull()).toBeNull();
});

// For other values:
    // boBeUndefined
    // toBeDefined
    // toBeTruthy
    // toBeFalsy

// model simple test falsy
test('Should be falsy', () =>{
    expect(functions.checkValue(undefined)).toBeFalsy();
});

// model object test equal
test('User should be Brad Traversy object',() =>{
    expect(functions.createUser()).toEqual({
        firstName: 'Brad',
        lastName: 'Traversy'
    })
})

// model test Less than and greater than
// for greater than:
    // LessThan -> GreaterThan
test('Should be under 1600', () =>{
    const load1 = 800;
    const load2 = 700;
    expect(load1 + load2).toBeLessThanOrEqual(1600);
});

test('There is no I in team', ()=>{
    expect('teami').not.toMatch(/I/);
})

// model test Array

test('Admin should be in usernames', () =>{
    usernames = ['john', 'karen', 'admin']
    expect(usernames).toContain('admin');
})

// model test with promise
/* test ('User fetched name should be Leanne Graham', () => {
    expect.assertions(1);
    return functions.fetchUser().then(data =>{
        expect(data.name).toEqual('Leanne Graham');
    });
});
 */
// model test with async Await
test ('User fetched name should be Leanne Graham', async () => {
    expect.assertions(1);
    const data = await functions.fetchUser();
    expect(data.name).toEqual('Leanne Graham');
});

// beforeEach(() => initDatabase());
// afterEach(() => closeDatabase());

// beforeAll(() => initDatabase());
// afterAll(() => closeDatabase());

// const initDatabase = () => console.log('Database Initialized ...');
// const closeDatabase = () => console.log('Database Closed ...');
const nameCheck = () => console.log('Checking Name ...')

describe('Checking Names', () => {
    beforeEach(() => nameCheck());

    test('User is Jeff', () =>{
        const user = 'Jeff';
        expect(user).toBe('Jeff')
    })

    test('User is karen', () =>{
        const user = 'karen';
        expect(user).toBe('karen')
    })

})

/* 
Instalando o Jest

        yarn add -D jest

    Basta add no package.json o test da seguinte maneira

        "scripts": {
            "test": "jest",
        }
*/

/* 
Melhorando a experiencia com o Jest
    
    Watch Usage:
        Basta add no package.json o testwatch da seguinte maneira, além de acrescentar 
        algumas funcionalidades ele também roda os tests a cada novo save
    
            "scripts": {
                "test": "jest",
                "testwatch":"jest --watchAll"
            }

        basta rodar no terminal:

            yarn testwatch
    
    
    
    Adicionar jest.config.js:
    
        yarn jest --init

    Coverage Reporter:
        Abra a pasta jest.config.js
        uncommment e substituir false por true:

            collectCoverage: true,

        uncomment:
        
            coverageDirectory: "coverage",

        uncommet e substituir undefined pela pasta que será testada:

            collectCoverageFrom: ["scr/**"],

        caso não queira retirar uma pasta dos tests, basta add a collectCoverageFrom "!" + "pasta ignorada"

            collectCoverageFrom: ["scr/**","!pastaquenaoquero"],
            

            





*/
      